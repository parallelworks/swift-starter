#! /bin/bash

export GLOBUS_HOSTNAME=54.172.106.88
export GLOBUS_TCP_PORT_RANGE=4000,4050
export GLOBUS_TCP_SOURCE_RANGE=4000,4050

coaster-service -p 4015 -localport 4016 -stats -nosec -passive
