# Swift Workflow for Blender Cycles Batch Rendering

This package details a Swift workflow for running Blender Cycles batch rendering. The workflow reads a blender file with an animation path, and a tarred file of the model textures, and outputs a tar files of the rendered images, and an animated GIF image.

* * *

**RUN INSTRUCTIONS**

On the headnode (where the workflow will be run from), clone the workflow with the following command:

```
git clone git@bitbucket.org:parallelworks/blender_swift.git
```

Install imagemagick for converting renderings to animations:

```
sudo apt-get install imagemagick
```

Add Swift to your path with the following command:

```
export PATH=$PATH:$PWD/swift/bin
```

You could run the workflow locally by changing sites: [coasters] in swift.conf to sites: [local], or follow the instructions below to run with a coaster service.


Start the headnode coaster service with the following command - ensure the GLOBUS_HOSTNAME variable in the startCoaster.sh file matches your public IP Address:

```
./coasters/startCoaster.sh
```

On worker nodes (could run on the same local machine for now), install docker and pull the ikester/blender docker container, and start the worker script to connect to the headnode coaster service:

```
# install docker and pull the blender container
wget -qO- https://get.docker.com/ | sh
docker pull ikester/blender

# start the coaster worker service
./coasters/startWorker.sh
```

Check the headnode coaster stats and ensure the worker nodes are connected. You should see something similar to below:

![Sample Coaster Stats](https://bytebucket.org/parallelworks/openfoamsweep/raw/d76058364ffe035cbd80d4b2d969b6fd8cf06642/coaster_tasks/sampleStats.png)


Once workers are connected to the resource pool, on the headnode start the workflow with the following command:

```
./start.sh
```

or to run manually

```
# ./main.sh <blender_file> <texture_tar> <num_frames_to_render> <output_tar_filename> <output_animation_filename>

./main.sh sample_inputs/pavillon.blend sample_inputs/textures.tgz 3 renders.tgz animation.gif
```

Upon run completion, an animation is created and the individual images are tarred in a final file chosen in the execution line.