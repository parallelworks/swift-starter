type file;

app(file out) render (file render,file blend, int frame, file textures, file renderpy){
    bash @render @blend frame @out;
}

file render     <single_file_mapper; file="renderFrame.sh">;
file renderpy   <single_file_mapper; file="renderFrame.py">;
file blend      <single_file_mapper; file="in.blend">;
file textures   <single_file_mapper; file="textures.tgz">;

string maxframe = arg("frames","10");

file[] frames;

foreach f in [1:toInt(maxframe)]{
    file rendering <single_file_mapper; file=strcat("results/","out_",f,".png")>;  
    rendering = render(render,blend,f,textures,renderpy);
}
